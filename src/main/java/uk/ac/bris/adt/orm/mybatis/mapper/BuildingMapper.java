package uk.ac.bris.adt.orm.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import uk.ac.bris.adt.orm.mybatis.model.Building;

import java.util.List;

@Mapper
public interface BuildingMapper {
  List<Building> findAll();

  Building findOne(Integer id);

  void insert(Building building);

  void update(Building building);

  void delete(Building building);
}

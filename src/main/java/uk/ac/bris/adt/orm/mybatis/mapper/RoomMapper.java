package uk.ac.bris.adt.orm.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import uk.ac.bris.adt.orm.mybatis.model.Room;

@Mapper
public interface RoomMapper {
  Room findOne(Integer id);

  void insert(Room room);

  void update(Room room);

  void delete(Room room);
}

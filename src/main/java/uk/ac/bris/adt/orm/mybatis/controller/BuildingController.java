package uk.ac.bris.adt.orm.mybatis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import uk.ac.bris.adt.orm.mybatis.mapper.BuildingMapper;
import uk.ac.bris.adt.orm.mybatis.model.Building;

import java.util.List;

@Controller
@RequestMapping("/buildings")
public class BuildingController {
  private final BuildingMapper buildingMapper;

  public BuildingController(BuildingMapper buildingMapper) {
    this.buildingMapper = buildingMapper;
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.setAllowedFields("name");
  }

  @GetMapping
  public String index(Model model) {
    List<Building> buildingList = buildingMapper.findAll();
    model.addAttribute(buildingList);
    return "buildings/index";
  }

  @GetMapping("/{id}")
  public String show(@PathVariable Integer id, Model model) {
    Building building = buildingMapper.findOne(id);
    model.addAttribute(building);
    return "buildings/show";
  }

  @GetMapping("/add")
  public String add(Model model) {
    Building building = new Building();
    model.addAttribute(building);
    return "buildings/add";
  }

  @PostMapping
  public String create(@ModelAttribute Building buildingForm, RedirectAttributes redirectAttributes) {
    buildingMapper.insert(buildingForm);
    redirectAttributes.addFlashAttribute("message", "Created building " + buildingForm.getName());
    return "redirect:/buildings/" + buildingForm.getId();
  }

  @GetMapping("/{id}/edit")
  public String edit(@PathVariable Integer id, Model model) {
    Building building = buildingMapper.findOne(id);
    model.addAttribute(building);
    return "buildings/edit";
  }

  @PostMapping("/{id}")
  public String update(@PathVariable Integer id, @ModelAttribute Building buildingForm,
                       RedirectAttributes redirectAttributes) {
    Building building = buildingMapper.findOne(id);
    building.setName(buildingForm.getName());
    buildingMapper.update(building);
    redirectAttributes.addFlashAttribute("message", "Updated building " + building.getName());
    return "redirect:/buildings/" + building.getId();
  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable Integer id, RedirectAttributes redirectAttributes) {
    Building building = buildingMapper.findOne(id);
    buildingMapper.delete(building);
    redirectAttributes.addFlashAttribute("message", "Deleted building " + building.getName());
    return "redirect:/buildings";
  }
}
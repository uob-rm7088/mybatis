package uk.ac.bris.adt.orm.mybatis.model;

public class Room {
  private Integer id;
  private String name;
  private Building building;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Building getBuilding() {
    return building;
  }

  public void setBuilding(Building building) {
    this.building = building;
  }

  @Override
  public String toString() {
    return name;
  }
}

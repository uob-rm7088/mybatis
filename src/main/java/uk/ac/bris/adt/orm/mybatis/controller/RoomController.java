package uk.ac.bris.adt.orm.mybatis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import uk.ac.bris.adt.orm.mybatis.mapper.BuildingMapper;
import uk.ac.bris.adt.orm.mybatis.mapper.RoomMapper;
import uk.ac.bris.adt.orm.mybatis.model.Building;
import uk.ac.bris.adt.orm.mybatis.model.Room;

@Controller
@RequestMapping("/rooms")
public class RoomController {
  private final BuildingMapper buildingMapper;
  private final RoomMapper roomMapper;

  public RoomController(BuildingMapper buildingMapper, RoomMapper roomMapper) {
    this.buildingMapper = buildingMapper;
    this.roomMapper = roomMapper;
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.setAllowedFields("name");
  }

  @GetMapping("/add")
  public String add(@RequestParam("building.id") Integer id, Model model) {
    Building building = buildingMapper.findOne(id);
    Room room = new Room();
    room.setBuilding(building);
    model.addAttribute(room);
    return "rooms/add";
  }

  @PostMapping
  public String create(@RequestParam("building.id") Integer id, @ModelAttribute Room roomForm,
                       RedirectAttributes redirectAttributes) {
    Building building = buildingMapper.findOne(id);
    roomForm.setBuilding(building);
    roomMapper.insert(roomForm);
    redirectAttributes.addFlashAttribute("message", "Added room " + roomForm + " to building " + roomForm.getBuilding());
    return "redirect:/buildings/" + roomForm.getBuilding().getId();
  }

  @GetMapping("/{id}/edit")
  public String edit(@PathVariable Integer id, Model model) {
    Room room = roomMapper.findOne(id);
    model.addAttribute(room);
    return "rooms/edit";
  }

  @PostMapping("/{id}")
  public String update(@PathVariable Integer id, @ModelAttribute Room roomForm, RedirectAttributes redirectAttributes) {
    Room room = roomMapper.findOne(id);
    room.setName(roomForm.getName());
    roomMapper.update(room);
    redirectAttributes.addFlashAttribute("message", "Updated room " + room + " in building " + room.getBuilding());
    return "redirect:/buildings/" + room.getBuilding().getId();
  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable Integer id, RedirectAttributes redirectAttributes) {
    Room room = roomMapper.findOne(id);
    roomMapper.delete(room);
    redirectAttributes.addFlashAttribute("message", "Removed room " + room + " from building " + room.getBuilding());
    return "redirect:/buildings/" + room.getBuilding().getId();
  }
}
package uk.ac.bris.adt.orm.mybatis.model;

import java.util.List;

public class Building {
  private Integer id;
  private String name;
  private List<Room> rooms;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Room> getRooms() {
    return rooms;
  }

  public void setRooms(List<Room> rooms) {
    this.rooms = rooms;
  }

  @Override
  public String toString() {
    return name;
  }
}

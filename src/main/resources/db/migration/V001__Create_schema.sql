CREATE TABLE building (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR2(100) UNIQUE NOT NULL
);

CREATE TABLE room (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR2(100) NOT NULL,
  building_id INTEGER NOT NULL CONSTRAINT room_building_fk REFERENCES building(id) ON DELETE CASCADE,
  UNIQUE (name, building_id)
);

INSERT INTO building (name) VALUES ('Howard House');
INSERT INTO building (name) VALUES ('Richmond Building');
INSERT INTO building (name) VALUES ('Computer Centre');

INSERT INTO room (name, building_id) VALUES ('Cagney Room', 1);
INSERT INTO room (name, building_id) VALUES ('Lacey Room', 1);
INSERT INTO room (name, building_id) VALUES ('Castle Room', 2);
INSERT INTO room (name, building_id) VALUES ('Conference Room', 3);

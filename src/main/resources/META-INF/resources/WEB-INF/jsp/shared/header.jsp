<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>ORM</title>

  <link href="<c:url value="/webjars/bootstrap/3.3.7/dist/css/bootstrap.min.css"/>" rel="stylesheet">
  <link href="<c:url value="/webjars/bootstrap/3.3.7/dist/css/bootstrap-theme.min.css"/>" rel="stylesheet">
  <link href="<c:url value="/static/css/app.css"/>" rel="stylesheet">
</head>

<body data-context-path="${pageContext.request.contextPath}">

  <div class="container" role="main">

